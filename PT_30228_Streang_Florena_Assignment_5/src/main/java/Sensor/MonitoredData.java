package Sensor;

public class MonitoredData {
    private String start_time,end_time,activity_label;
    private int dayoftheYear;

    public void setDayoftheYear(int dayoftheYear) {
        this.dayoftheYear = dayoftheYear;
    }

    public int getDayoftheYear() {
        return dayoftheYear;
    }

    public MonitoredData(String start_time, String end_time, String activity_label) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity_label = activity_label;
    }
    public String getStart_time() { return start_time; }
    public String getEnd_time() { return end_time; }
    public String getActivity_label() { return activity_label; }



}
