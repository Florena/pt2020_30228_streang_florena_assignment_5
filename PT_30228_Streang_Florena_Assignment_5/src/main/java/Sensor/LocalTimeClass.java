package Sensor;

import java.time.LocalTime;

public class LocalTimeClass {
    private LocalTime localTime;
    private int days;

    public LocalTimeClass(LocalTime localTime,int days){
        this.localTime=localTime;
        this.days=days;
    }

    public LocalTimeClass calculate(LocalTime t1){
        LocalTime aux=localTime;
        localTime= localTime.plusHours(t1.getHour())
                .plusMinutes(t1.getMinute())
                .plusSeconds(t1.getSecond());
        if(localTime.isBefore(aux)){
            days++;
        }
        return this;
    }

    public void setLocalTime(LocalTime localTime) {
        this.localTime = localTime;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public LocalTime getLocalTime() {
        return localTime;
    }

    public int getDays() {
        return days;
    }
}
