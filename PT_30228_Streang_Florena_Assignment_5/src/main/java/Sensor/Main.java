package Sensor;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IOException {
        ReadWriteData rd=new ReadWriteData();
        Counter counter=new Counter();
        List<MonitoredData> monitoredData;
        Map<String,Integer> map;
        Map<Integer,Map<String,Integer>> mapMap;
        Map<String, LocalTimeClass> localTimeMap;
        monitoredData=rd.read();
        rd.write(monitoredData,1,0,null,null,null);
        rd.write(monitoredData,2,counter.countDistinctDays(monitoredData),null,null,null);
        ActivityAppearances activityAppearances=new ActivityAppearances();
        map=activityAppearances.appearances(monitoredData);
        rd.write(monitoredData,3,0,map,null,null);
        mapMap=activityAppearances.appearancesperday(monitoredData);
        rd.write(monitoredData,4,0,null,mapMap,null);
        localTimeMap=counter.durationActivity(monitoredData);
        rd.write(monitoredData,5,0,null,null,localTimeMap);


    }
}
