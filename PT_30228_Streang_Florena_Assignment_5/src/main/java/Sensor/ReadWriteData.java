package Sensor;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ReadWriteData {


        public List<MonitoredData> read() throws IOException {
            List<MonitoredData> monitoredData = new ArrayList<>();
            List<String[]> stringList;
            stringList = Files.lines(Paths.get("src\\main\\java\\Sensor\\activity"))
                    .map(String -> String.split("\t\t")).collect(Collectors.toList());
            for (String[] s : stringList) {

                String start = s[0];
                String end = s[1];
                String activity = s[2];
                monitoredData.add(new MonitoredData(start, end, activity));

            }
            return monitoredData;
        }

        public void write(List<MonitoredData> monitoredData, int task, int days, Map<String,Integer> map, Map<Integer,Map<String,Integer>> mapMap, Map<String, LocalTimeClass> timeClassMap) throws IOException {
            FileOutputStream fos = new FileOutputStream("task"+task+".txt");
            DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(fos));
            if(task==1) {
                for (MonitoredData data : monitoredData) {
                    outStream.writeChars(data.getStart_time() + "\t" + data.getEnd_time() + "\t" + data.getActivity_label() + "\n");
                }
            }
            else
                if(task==2)
                    outStream.writeChars(String.valueOf(days));
                else
                    if(task==3){
                        map.forEach((K,V)-> {
                            try {
                                outStream.writeChars(K.toString() + " " + V.toString() + "\n");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    else
                        if(task==4){
                            mapMap.forEach((K,V)->{
                                try {
                                    outStream.writeChars("\nDay "+K.toString()+"\n\n");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                V.forEach((X,Y)->{
                                    try {
                                        outStream.writeChars(X+" "+Y +"\n");
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                });
                            });

                        }
                        else if(task==5){
                            timeClassMap.forEach((K,V)-> {
                                try {
                                    outStream.writeChars(K.toString() +" "+V.getDays()+ " days " + V.getLocalTime().toString()+ "\n");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            });
                        }
            outStream.close();
        }
    }


