package Sensor;

import java.util.*;

public class ActivityAppearances {

        public Map<String, Integer> appearances(List<MonitoredData> list){
            Map<String, Integer> map= new HashMap<>();
            String[] activities={"Leaving", "Toileting", "Showering", "Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
            Set<String> keywordSet = new HashSet<>(Arrays.asList(activities));
            for (MonitoredData m:list) {
                if(keywordSet.contains(m.getActivity_label())){
                    Integer value = map.get(m.getActivity_label());
                    if (value == null)
                        value = 0;
                    value++;
                    map.put(m.getActivity_label(), value);
                }
            }
         return map;
        }


        public Map<Integer, Map<String, Integer>> appearancesperday(List<MonitoredData> list){
            Map<Integer,Map<String,Integer>> mapMap=new HashMap<>();
            for (MonitoredData m:list) {
                if(!(mapMap.containsKey(m.getDayoftheYear())))
                {
                    Map<String,Integer> map=new HashMap<>();
                    map.put(m.getActivity_label(),1);
                    mapMap.put(m.getDayoftheYear(),map);
                }
                else
                {
                    Map<String,Integer> map=mapMap.get(m.getDayoftheYear());
                    if(map==null) map.put(m.getActivity_label(), 0);
                    else {
                        Integer value = map.get(m.getActivity_label());
                        if (value == null)
                            value = 0;
                        value++;
                        map.put(m.getActivity_label(), value);
                    }
                   mapMap.replace(m.getDayoftheYear(),map);
                }
            }
            return mapMap;
        }
}

