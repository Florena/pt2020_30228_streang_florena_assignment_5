package Sensor;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Counter {
    public int countDistinctDays(List<MonitoredData> monitoredData){
        int numberofDays=0;
        int max=0;
        int month=0;
        DateTimeFormatter f=DateTimeFormatter.ofPattern ( "yyyy-MM-dd" );
        for (MonitoredData m:monitoredData) {
            String[] data=m.getStart_time().split(" ");
            CharSequence data2=data[0];
            LocalDate odt = LocalDate.parse(data2,f);
            m.setDayoftheYear(odt.getDayOfYear());
            if (odt.getDayOfYear()>max){
                max=odt.getDayOfYear();
                numberofDays++;
            }
        }
        return numberofDays;
    }

    public LocalTime diff(String start_time,String end_time){

        LocalDateTime start,end;
        DateTimeFormatter formatter= DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        start=LocalDateTime.parse(start_time,formatter);
        end=LocalDateTime.parse(end_time,formatter);
        LocalTime localTime=LocalTime.of(0,0,0);
        localTime=localTime.plusSeconds(Duration.between(start,end).getSeconds());
        return localTime;
    }



    public Map<String, LocalTimeClass> durationActivity(List<MonitoredData> list){
        Map<String, LocalTimeClass> timeHashMap=new HashMap<>();
        String[] activities={"Leaving", "Toileting", "Showering", "Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
        Set<String> keywordSet = new HashSet<>(Arrays.asList(activities));
        for (MonitoredData m:list) {
                if(keywordSet.contains(m.getActivity_label())) {
                    LocalTimeClass value = timeHashMap.get(m.getActivity_label());
                    if (value == null) {
                        value=new LocalTimeClass(LocalTime.parse("00:00:00"),0);
                    }
                    value = value.calculate(diff(m.getStart_time(),m.getEnd_time()));
                    timeHashMap.put(m.getActivity_label(), value);

                }
            }
            return timeHashMap;

        }
        }


